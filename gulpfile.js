const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const browserify = require('browserify');
const watchify = require('watchify');
const gutil = require('gulp-util');

gulp.task('build', function() {
	return browserify('./src/client/app.jsx', { debug: true})
		.transform('babelify', {presets: ['es2015', 'react']})
		.bundle()
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('./build/'));
});


gulp.task('watch', function(){
	let bundler =  browserify('./src/client/app.jsx', { 
			cache: {}, 
			packageCache: {}, 
			plugin: [watchify],
			debug: true
		});
    
		bundler.on('update', () => bundle(bundler));
        bundler.on('log', console.log);
        bundler.on('error', console.log);
        bundle(bundler);

	return bundler;
});


function bundle(bundler = this){
    bundler
        .transform('babelify', {presets: ['es2015', 'react']})
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('./build/'));
}