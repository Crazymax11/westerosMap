const App = require('./src/server/app');

const commandLineArgs = require('command-line-args')
const optionDefinitions  = [
    {
        name: 'port',
        alias: 'p',
        type: Number
    },

    {
        name: 'host',
        alias: 'h',
        type: String
    },

    {
        name: 'config',
        alias: 'c',
        type: String
    },

    {
        name: 'static',
        alias: 's',
        type: String
    }
];

const options = commandLineArgs(optionDefinitions);

options.host = options.host || 'localhost';
options.port = options.port || 80;
options.static = options.static || './static';

new App( (options.config && require(options.config)) || options);