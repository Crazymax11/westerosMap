const express = require('express');

class App{
    constructor(options){
        let app = express();
        app.use(express.static(options.static));

        this._configureRoutes(app);

        app.listen(options.port, options.host);

        this.app = app;
    }   

    _configureRoutes(app){
        return this;
    }
}

module.exports = App;