import { Component } from 'redux';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import configureStore from './store/configureStore.js';
import React from 'react';

import WestersoMap from './components/map/container.jsx';

import regionsInitialState from './store/regions/initialState.js';

const APP_SELECTOR_ID = 'app';

const store = configureStore({
	regions: regionsInitialState
});

ReactDOM.render(
	<Provider store={store}>
			<WestersoMap/>
	</Provider>,
	document.getElementById(APP_SELECTOR_ID)
);