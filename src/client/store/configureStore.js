import { createStore,  applyMiddleware } from 'redux';
import { combineReducers } from 'redux';
import regions from './regions/reducer.js';


export default function configureStore(initialState = {}) {
  const store = createStore(
    combineReducers({
      regions
    }), 
    initialState);
  
  
  return store;
}