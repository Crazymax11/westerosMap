import { constants } from './actions.js';

const {STARK, LANNISTER, GREYJOY, TYRELL, MARTELL, BARATHEON}  = {
    STARK: 'stark',
    LANNISTER: 'lannister',
    GREYJOY: 'greyjoy',
    TYRELL: 'tyrell',
    MARTELL: 'martell',
    BARATHEON: 'baratheon'
};

const housesRotation = [
    STARK, LANNISTER, GREYJOY, TYRELL, MARTELL, BARATHEON
];




export default function reducer(state = {}, action){
	switch (action.type){
		case constants.CHANGE_REGION_OWNER:
			return changeRegionOwner(state, action.payload);
        case constants.CHANGE_REGION_OWNER_TO:
            return changeRegionOwnerTo(state, action.payload);
		default:
			return state;
	}
}

function changeRegionOwner(state, payload){
	let newState = Object.assign([], state);
	let targetRegion = newState.find((item)=>{
        return item.name == payload.name;
    });

    let nextOwner = housesRotation[0];

    if (targetRegion.owner) {
        nextOwner = housesRotation[housesRotation.indexOf(targetRegion.owner)+1];
    }

	targetRegion.owner = nextOwner;
	return newState;
}

function changeRegionOwnerTo(state, { region, owner = null} = {}){

    if (!region) {
        return state;
    }
    
    let newState = Object.assign([], state);

    let regionToSetOwner = newState.find((item)=>{
        return item.name == region;
    });

    regionToSetOwner.owner = owner;

    return newState;
}