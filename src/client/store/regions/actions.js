const constants = {
	CHANGE_REGION_OWNER: 'CHANGE_REGION_OWNER',
    CHANGE_REGION_OWNER_TO: 'CHANGE_REGION_OWNER_TO'
}


function changeRegionOwner(name){
	return {
		type: constants.CHANGE_REGION_OWNER,
		payload: {
			name
		}
	}
}


function changeRegionOwnerTo(region, owner){
    return {
        type: constants.CHANGE_REGION_OWNER_TO,
        payload: {
            region,
            owner
        }
    }
}

export {
	constants,
	changeRegionOwner,
    changeRegionOwnerTo
} 


