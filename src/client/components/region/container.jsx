import Region from './component.jsx';
import {connect} from 'react-redux';
import {changeRegionOwnerTo} from  '../../store/regions/actions';


function mapStateToProps(state, ownProps){
	return {ownProps};
}

function mapDispatchToProps(dispatch, ownProps){
	return {
		changeRegionOwner: (name) => {
			return dispatch(changeRegionOwnerTo(ownProps.name, name))
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Region);