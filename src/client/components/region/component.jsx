import { Component } from 'react';
import React from 'react';
import OwnerChooser from '../ownerChooser/component.jsx';


export default class Region extends Component{
    constructor(props){
        super(props);
        this.state = {};
    }


	render(){
        let state = this.state;
		return (<path 
                d={this.props.path}
                className={`westeros-region ${this.props.owner}-region`}
                onClick={this.props.chooseOwnerClicked}
                >
            </path>)
	}

    getPathCenterPosition(){
        let acc = this.props.path
            .split(' ')
            .reduce( (acc, chunk) => {
                let [x,y] = chunk.split(',');

                if (typeof y == 'undefined') {
                    return acc;
                }

                const getLast = (arr) => arr[arr.length-1] || 0;

                acc.x.push(getLast(acc.x) + Number(x));
                acc.y.push(getLast(acc.y) + Number(y));

                return acc;
            }, {x: [], y: []});
        return {
            x: (Math.max.apply(null, acc.x) + Math.min.apply(null, acc.x))/2,
            y: (Math.max.apply(null, acc.y) + Math.min.apply(null, acc.y))/2,
        }
    }
}
