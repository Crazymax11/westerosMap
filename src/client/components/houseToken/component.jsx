import React, {Component} from 'react';

const BASE_URL = './images/Houses/';
const EXTENSION = '.png'

export default class extends Component {
    render(){
        return (
            <div className="house-token" onClick={() => this.props.clicked(this.props.name)}>
                <img src={`${BASE_URL}${this.props.name}${EXTENSION}`}/>
            </div>);
    }
}