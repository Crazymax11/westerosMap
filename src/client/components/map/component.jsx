import React from 'react';
import { Component } from 'react';
import Region from '../region/container.jsx';
import OwnerChooser from '../ownerChooser/component.jsx';

const SVG_ATTRIBUTES = {
			xmlnsDc: 'http://purl.org/dc/elements/1.1/',
			xmlnsCc: 'http://creativecommons.org/ns#',
			xmlnsRdf: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
			xmlnsSvg: 'http://www.w3.org/2000/svg',
			xmlns: 'http://www.w3.org/2000/svg',
			xmlnsXlink: 'http://www.w3.org/1999/xlink',
			xmlnsSodipodi: 'http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd',
			xmlnsInkscape: 'http://www.inkscape.org/namespaces/inkscape',
			viewBox: '0 0 2475 3718',
			id: 'westerosMapLayout',
			version: '1.1',
			inkscapeVersion: '0.91 r13725',
			sodipodiDocname: 'westeros.svg',
		};

/**
 * Заправляет всей дичью, знает о всех слоях, но слои друг о друге не догадываются.
 * По возможности работает с чистым свг
 */
export default class WesterosMapLayout extends Component{
	constructor(props){
		super(props);
		this.state = {		
			choosing : {}		
		};
	}
	

	render(){
		let {regions: regions = []} = this.props;
		return (
			<svg
				{...SVG_ATTRIBUTES}
			>
				<sodipodiNamedview
				    id="base"
				    pagecolor="#ffffff"
				    bordercolor="#666666"
				    borderopacity="1.0"
				    showgrid="false" />
				<metadata id="metadata7">
				    <rdfRDF>
				      <ccWork rdfAbout="">
				        <dcFormat>image/svg+xml</dcFormat>
				        <dcType rdfResource="http://purl.org/dc/dcmitype/StillImage"/>
				        <dcTitle />
				      </ccWork>
				    </rdfRDF>
				</metadata>
				<g id="image-layer">
				  	<image
					    style={{fill:'#e9afaf'}}
					    width="2475"
					    height="3718.75"
					    preserveAspectRatio="none"
					    xlinkHref="/images/map.jpg"
					    id="image3344"
					    x="1.3491075e-009"
					    y="2.9403801">
				    </image>
				</g>
				<g id="paths-layer">
				  	{ 
			    		regions.map( region => {
			    			return <Region 
			    				{...region} 
			    				chooseOwnerClicked={(e) => {
			    					const position = calculatePosition(e);
			    					const choosing = this.state.choosing;
			    					choosing[region.name] = position;

			    					this.setState({
			    						choosing
			    					});
			    				}}/>;	
			    		})	
				    }
				</g>
				<g id="map-objects-layer">
				</g>
				<g id="orders-layer">
				</g>
				<g id="armies-layer">
				</g>
				<g id="owner-chooser-layer">
					{
					  	Object.keys(this.state.choosing)
					  		.map( (name) => {
					  			let choosing = this.state.choosing[name];
					  			console.log(choosing);
					  			return <foreignObject {...choosing} className="region-owner-chooser">
			                        <OwnerChooser ownerChosen={(owner) => {
			                            let choosing = this.state.choosing || {};
				    					delete choosing[name];
				    					this.setState({choosing});
			                            this.props.changeRegionOwnerTo(name, owner);
			                        }}/>
			                    </foreignObject>
					  		})
				  }
				  </g>
       </svg>
       );
	}
}


function calculatePosition(event){
	let svg = document.getElementById('westerosMapLayout');
	let pt = svg.createSVGPoint();
	pt.x = event.clientX; pt.y = event.clientY;

	let {x,y} = pt.matrixTransform(svg.getScreenCTM().inverse());
	
	return {
		x,y 
	}
}