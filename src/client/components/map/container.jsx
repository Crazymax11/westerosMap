import Component from './component.jsx';
import {connect} from 'react-redux';
import {changeRegionOwnerTo} from '../../store/regions/actions.js';

function mapStateToProps(state, ownProps){
	return {
		regions: state.regions
	}
}

function mapDistapchToProps(dispatch, ownProps){
    return {
        changeRegionOwnerTo: (region, owner) => {
            return dispatch(changeRegionOwnerTo(region, owner))
        }
    }
}

export default connect(mapStateToProps, mapDistapchToProps)(Component);

