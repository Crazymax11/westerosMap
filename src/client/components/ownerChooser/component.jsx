import React, {Component} from 'react';
import HouseToken from '../houseToken/component.jsx';

const HOUSES_NAMES = [
    'tyrell',
    'baratheon',
    'greyjoy',
    'lannister',
    'stark',
    'martell'
];

export default class OwnerChooser extends Component{
    render(){
        return <div className="owner-chooser">
            { HOUSES_NAMES.map( (name) => {
                return <HouseToken  key={name} name={name} clicked={this.props.ownerChosen}/>
            })}
        </div>
    }
}