//TODO: не юзается, но лейаут под роутер будущий может пригодится как отдельный компонент

import React from 'react';
import { Component } from 'react';
import Region from './region/regionComponent.jsx';

export default class WesterosMapLayout extends Component{
	render(){
		let {regions: regions = []} = this.props;
		return <svg
			xmlnsDc="http://purl.org/dc/elements/1.1/"
			xmlnsCc="http://creativecommons.org/ns#"
			xmlnsRdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
			xmlnsSvg="http://www.w3.org/2000/svg"
			xmlns="http://www.w3.org/2000/svg"
			xmlnsXlink="http://www.w3.org/1999/xlink"
			xmlnsSodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
			xmlnsInkscape="http://www.inkscape.org/namespaces/inkscape"
			viewBox="0 0 2475 3718"
			id="svg2"
			version="1.1"
			inkscapeVersion="0.91 r13725"
			sodipodiDocname="westeros.svg">
		<sodipodiNamedview
	     id="base"
	     pagecolor="#ffffff"
	     bordercolor="#666666"
	     borderopacity="1.0"
	     showgrid="false" />
		  <metadata
		     id="metadata7">
		    <rdfRDF>
		      <ccWork
		         rdfAbout="">
		        <dcFormat>image/svg+xml</dcFormat>
		        <dcType
		           rdfResource="http://purl.org/dc/dcmitype/StillImage" />
		        <dcTitle />
		      </ccWork>
		    </rdfRDF>
		  </metadata>
		  <g
		     id="layer1"
		     >
		    <image
		       style={{fill:'#e9afaf'}}
		       width="2475"
		       height="3718.75"
		       preserveAspectRatio="none"
		       xlinkHref="./map.jpg"
		       id="image3344"
		       x="1.3491075e-009"
		       y="2.9403801">
		    </image>
		    { 
	    		regions.map( regionName => {
	    			return <Region name={regionName}/>;	
	    		})	
		    }
	       </g>
       </svg>
	}
}